package Helper;

public class UpdateOrderIndentHeaders {
	private String creditBasisAsOn;

	private String prioritization;

	private String letterHeading;

	private String brandType;

	private String buyerId;
	
	private String seasonYear;
	
	private String seasonId;
	
	private String commercialType;
	
	private String buyPlanOrderType;
	
	private String categoryManagerEmail;
	
	private String comments;
	
	private String mailText;
	
	
	

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getMailText() {
		return mailText;
	}

	public void setMailText(String mailText) {
		this.mailText = mailText;
	}

	public String getSeasonYear() {
		return seasonYear;
	}

	public void setSeasonYear(String seasonYear) {
		this.seasonYear = seasonYear;
	}

	public String getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(String seasonId) {
		this.seasonId = seasonId;
	}

	public String getCommercialType() {
		return commercialType;
	}

	public void setCommercialType(String commercialType) {
		this.commercialType = commercialType;
	}

	public String getBuyPlanOrderType() {
		return buyPlanOrderType;
	}

	public void setBuyPlanOrderType(String buyPlanOrderType) {
		this.buyPlanOrderType = buyPlanOrderType;
	}

	public String getCategoryManagerEmail() {
		return categoryManagerEmail;
	}

	public void setCategoryManagerEmail(String categoryManagerEmail) {
		this.categoryManagerEmail = categoryManagerEmail;
	}

	public String getCreditBasisAsOn() {
		return creditBasisAsOn;
	}

	public void setCreditBasisAsOn(String creditBasisAsOn) {
		this.creditBasisAsOn = creditBasisAsOn;
	}

	public String getPrioritization() {
		return prioritization;
	}

	public void setPrioritization(String prioritization) {
		this.prioritization = prioritization;
	}

	public String getLetterHeading() {
		return letterHeading;
	}

	public void setLetterHeading(String letterHeading) {
		this.letterHeading = letterHeading;
	}

	public String getBrandType() {
		return brandType;
	}

	public void setBrandType(String brandType) {
		this.brandType = brandType;
	}

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}
}
