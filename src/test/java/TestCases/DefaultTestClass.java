package TestCases;

import Helper.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.team.entry.ApplicationEntry;
import com.myntra.team.entry.TeamEntry;
import com.myntra.team.entry.TeamTypeEntry;
import com.myntra.test.commons.testbase.BaseTest;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;

/**
 * Created by 300000929 on 20/02/17.
 */
public class DefaultTestClass extends BaseTest {

    public Logger log = Logger.getLogger(DefaultTestClass.class);
    public BaseOrderIndentJson baseOI;
    public JeevesHelper jeeveshelper;
    public JeevesValidator jeevesvalidator;
    public VendorData benetton;
    public TeamServiceHelper teamserviceHelper;
    public TeamServiceValidator teamServiceValidator;
    public ObjectMapper mapper;
    
    @BeforeClass(alwaysRun = true)
    public void init()  throws Exception{

        baseOI = new BaseOrderIndentJson();
        jeeveshelper = new JeevesHelper();
        jeevesvalidator = new JeevesValidator();
        Constants.TEMPLATE_HEADERS.setColumnMap();
        benetton=new Constants.OIHeaderUCB();
		teamserviceHelper = new TeamServiceHelper();
		teamServiceValidator = new TeamServiceValidator();
		mapper = new ObjectMapper();
    }
}
