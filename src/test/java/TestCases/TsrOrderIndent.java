package TestCases;

import java.util.HashMap;
import java.util.List;

import com.myntra.tenant.client.entry.TenantEntry;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.lordoftherings.Initialize;
import com.myntra.lordoftherings.gandalf.APIUtilities;
import com.myntra.orderindent.enums.VendorTermsValidationStatus;
import com.myntra.test.commons.testbase.BaseTest;

import DataProvider.buyingandplanningDP;
import Helper.BaseOrderIndentJson;
import Helper.JeevesHelper;
import Helper.JeevesValidator;
import Helper.VendorData;
import RequestAndResponseClasses.BaseOIDetailsResponse;
import Helper.Constants;
import Helper.Constants.BUSINESS_UNITS;
import Helper.Constants.DATA_FILES;
import Helper.Constants.HEADERS;
import Helper.Constants.PAYLOADS;
import Helper.Constants.ROLES;
import Helper.Constants.RUN_CONFIG_FILTERS;

import com.myntra.tenant.Response.TenantResponse;

/**
 * @author Ajay API Tests for TSR OrderIndents
 *
 */
public class TsrOrderIndent extends DefaultTestClass {

	@Test(enabled = true, dataProvider = "CreateTsrOi", dataProviderClass = buyingandplanningDP.class, groups = {
			"sanity", "order-indents" }, description = "Create OI & PI Test")
	public void create_pi_from_tsr_oi_test(String tenant, String config_query, String oi_query) throws Exception {

		 HashMap<String, String> download_rconfig_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "downloadreplfile");
		 HashMap<String, String> upload_rconfig_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "uploadfile");
		 HashMap<String, String> create_baseoi_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "create");
		 
		// Download tenant rconfig
		jeeveshelper.downloadRconfigForBuAndValidateBu(tenant, BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR,
				download_rconfig_headers);

		// upload the downloaded rconfig
		jeeveshelper.uploadRconfigForBu(DATA_FILES.downloaded_rconfig, tenant, upload_rconfig_headers);

		// Run rconfig and wait till it gets completed
		jeeveshelper.runTsrRconfigAndWaitTillSuccess(tenant, BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR, config_query,
				create_baseoi_headers);

		// GENERATE OI
		jeeveshelper.generateOIAndWaitTillSuccess(tenant, PAYLOADS.MENS_CAUSAL_FOOTWEAR, oi_query,
				create_baseoi_headers);

		// get any OI ID for creating pi from the generated order indents
		String OI_ID = jeeveshelper.getOiData(DATA_FILES.OI_query, "id", create_baseoi_headers);
		
		//Search for the BuyPlan once job is Completed
        String OI_Search_Deatils = jeeveshelper.searchBuyPlan(OI_ID, create_baseoi_headers, "cm");
        BaseOIDetailsResponse baseOIDetailsResponse = (BaseOIDetailsResponse) APIUtilities.
                getJsontoObject(OI_Search_Deatils, new BaseOIDetailsResponse());
        
//        jeevesvalidator.validateBuyPlanSearchResponse(baseOIHeaderRequest, baseOIDetailsResponse, ROLES.CM_ROLE);
//
//        jeevesvalidator.validateTheVendorTermsValidationStatus(baseOIDetailsResponse, VendorTermsValidationStatus.PENDING);
//
//		// get VENDOR ID FOR vendor to cm mapping
//		String vendor_id = jeeveshelper.getOiData(DATA_FILES.OI_query, "vendorId", create_baseoi_headers);
//
//		// vendor to cm mapping for creating pi
//		jeeveshelper.vendorToCmMapping(vendor_id, "22", create_baseoi_headers);
//
//		// construct payload
//		String updateOI_headers_payload = baseOI.updateOIPayload(benetton);
//
//		// updateOI headers
//		String update_OI_response = jeeveshelper.updateOIHeaders(OI_ID, updateOI_headers_payload,
//				create_baseoi_headers);
//		jeevesvalidator.validateTheResponseStatus(update_OI_response, "UPDATE_OI_HEADERS");
//
//		// update OI status from draft to pending_with_partner
//		jeeveshelper.updateOIStatus(OI_ID, DATA_FILES.pending_with_partner_state, create_baseoi_headers, ROLES.CM_ROLE);
//
//		// update the OI status from pending_with_partner to pending_with_myntra
//		jeeveshelper.updateOIStatus(OI_ID, DATA_FILES.pending_with_myntra_state, create_baseoi_headers,
//				"role=vendor&vendorId=" + vendor_id);
//
//		// create PI
//		String create_pi_response = jeeveshelper.createPI(OI_ID, create_baseoi_headers);
//		jeevesvalidator.validateTheResponseStatus(create_pi_response, "CREATE_PI");

	}

	@Test(enabled = true, dependsOnMethods = "create_pi_from_tsr_oi_test", dataProvider = "BulkUploadTsrOi", dataProviderClass = buyingandplanningDP.class, groups = {
			"sanity", "order-indents" }, description = "Update OI & create PI Test")
	public void bulk_upload_of_tsr_oi_test(String tenant, String oi_query) throws Exception {

		HashMap<String, String> download_baseoi_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "downloadfile");
		
//		// Download tenant rconfig
//		jeeveshelper.downloadRconfigForBuAndValidateBu(tenant, BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR, download_rconfig_headers);
//
//		// upload the downloaded rconfig
//		jeeveshelper.uploadRconfigForBu(DATA_FILES.downloaded_file, tenant, upload_rconfig_headers);
//
//		// Run rconfig and wait till it gets completed
//		jeeveshelper.runTsrRconfigAndWaitTillSuccess(tenant, BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR, config_query,
//				create_baseoi_headers);

		// GENERATE OI
//		jeeveshelper.generateOIAndWaitTillSuccess(tenant, PAYLOADS.MENS_CAUSAL_FOOTWEAR, oi_query, create_baseoi_headers);

	/*	// get any OI ID for creating pi from the generated order indents
		String OI_ID = jeeveshelper.getOiData(DATA_FILES.OI_query, "id", create_baseoi_headers);

		// get VENDOR ID FOR vendor to cm mapping
		String vendor_id = jeeveshelper.getOiData(DATA_FILES.OI_query, "vendorId", create_baseoi_headers);

		// Download OI sheet
		jeeveshelper.downloadOI(OI_ID, download_baseoi_headers, ROLES.CM_ROLE, DATA_FILES.downloaded_file);

		// Update OI by uploading a modified oi sheet
		jeeveshelper.bulkUpdateOIAndWaitTillSuccess(Constants.DATA_FILES.downloaded_file, OI_ID, create_baseoi_headers,
				upload_baseoi_headers);

		// update the OI status from draft to pending_with_partner
		jeeveshelper.updateOIStatus(OI_ID, DATA_FILES.pending_with_partner_state, create_baseoi_headers, ROLES.CM_ROLE);

		// update the OI status from pending_with_partner to pending_with_myntra
		jeeveshelper.updateOIStatus(OI_ID, DATA_FILES.pending_with_myntra_state, create_baseoi_headers,
				ROLES.VENDOR_ROLE + vendor_id);

		// vendor to cm mapping
		jeeveshelper.vendorToCmMapping(vendor_id, ROLES.CM_ID, create_baseoi_headers);

		// construct payload
		String updateOI_headers_payload = baseOI.updateOIPayload(benetton);

		// updateOI headers
		String update_OI_response = jeeveshelper.updateOIHeaders(OI_ID, updateOI_headers_payload,
				create_baseoi_headers);
		jeevesvalidator.validateTheResponseStatus(update_OI_response, "UPDATE_OI_HEADERS");

		// create PI
		String create_pi_response = jeeveshelper.createPI(OI_ID, create_baseoi_headers);
		jeevesvalidator.validateTheResponseStatus(create_pi_response, "CREATE_PI");*/
	}

	@Test(enabled = true, dependsOnMethods = "create_pi_from_tsr_oi_test", dataProvider = "MultiTsrTenants", dataProviderClass = buyingandplanningDP.class, groups = {
			"sanity", "order-indents" }, description = "get summary and style data for BU in tenants")
	public void get_summary_and_style_data_for_bu_in_tenant(String tenant) throws Exception {

		HashMap<String, String> create_baseoi_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "create");
//		// Run rconfig and wait till it gets completed
//		jeeveshelper.runTsrRconfigAndWaitTillSuccess(tenant, BUSINESS_UNITS.SPORTS, config_query,
//				create_baseoi_headers);

		// get the summary data for the BU in tenant
		String summary_data = jeeveshelper.getSummaryDataForBu(tenant, BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR, DATA_FILES.filter,
				create_baseoi_headers);
		
		// validate summary is fetched or not 
		jeevesvalidator.validateResponseObjectArrayLength(jeeveshelper.getReplenishmentSummaryData(summary_data), "no summary data for the bu: " + BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR);
		
		// get the Replenishment set(styles) for the BU in tenant
		String replenishmentset_resp = jeeveshelper.getReplenishmentSet(tenant, BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR,
				create_baseoi_headers);
		
		// validate style is fetched or not 
		jeevesvalidator.validateResponseObjectArrayLength(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp), "no style data for the bu: " + BUSINESS_UNITS.MENS_CASUAL_FOOTWEAR);
		
		// validate total systemforcast count which should not be 0
		jeevesvalidator.checkForReplenishmentSystemForcast(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp));
	}
	
	/**
     * @param tenant
     * @param config_query
     * @param file
     * @throws Exception
     */
    @Test(enabled = true, dataProvider = "ReplenishmentSet", dataProviderClass = buyingandplanningDP.class, groups = {
            "sanity", "order-indents" }, description = "export replenishment set for BU in tenants")
    public void validate_run_config_filters_for_tenants(String tenant, String file, String config_query)
            throws Exception {
    	
    	HashMap<String, String> create_baseoi_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "create");
    	HashMap<String, String> upload_rconfig_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "uploadfile");
        // upload the rconfig
        jeeveshelper.uploadRconfigForBu(file, tenant, upload_rconfig_headers);

        // Run rconfig and wait till it gets completed
        jeeveshelper.runTsrRconfigAndWaitTillSuccess(tenant, BUSINESS_UNITS.MENS_CASUAL, config_query,
                create_baseoi_headers);

        // get the Replenishment set(styles) for the BU in tenant
        String replenishmentset_resp = jeeveshelper.getReplenishmentSet(tenant, BUSINESS_UNITS.MENS_CASUAL,
                create_baseoi_headers);

        // validate all the RCONFIG Header filter values
        jeevesvalidator.validateRconfigFilterValues(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp),
                "daysOnHand", RUN_CONFIG_FILTERS.DOH, "MAX");
        jeevesvalidator.validateRconfigFilterValues(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp),
                "rgmPerUnit", RUN_CONFIG_FILTERS.RGM, "MIN");
        jeevesvalidator.validateRconfigFilterValues(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp),
                "rateOfSale", RUN_CONFIG_FILTERS.ROS, "MIN");
        jeevesvalidator.validateRconfigFilterValues(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp),
                "discount", RUN_CONFIG_FILTERS.DISCOUNT, "MAX");
        jeevesvalidator.validateRconfigFilterValues(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp),
                "age", RUN_CONFIG_FILTERS.AGE, "MAX");
        jeevesvalidator.validateRconfigFilterValues(jeeveshelper.getReplenishmentStyleData(replenishmentset_resp),
                "rateOfReturns", RUN_CONFIG_FILTERS.RATE_OF_RETURNS, "MAX");

    }

	@Test(enabled = true, dependsOnMethods="validate_run_config_filters_for_tenants", dataProvider = "MultiTsrTenants", dataProviderClass = buyingandplanningDP.class, groups = {
			"sanity", "order-indents" }, description = "export replenishment set for BU in tenants")
	public void export_replenishmentset_for_bu_in_tenant(String tenant) throws Exception {

		HashMap<String, String> download_replenishmentSet_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "downloadreplfile");
		
		// Download replenishment set sheet
		jeeveshelper.downloadReplenishmentStyles(DATA_FILES.downloaded_file, download_replenishmentSet_headers, tenant,
				BUSINESS_UNITS.MENS_CASUAL);

		// Verify all the headers of the replenishment style sheet
		for (int i = 0; i < HEADERS.replenishment_style_sheet_headers.length; i++) {
			jeevesvalidator.validateTheFileData(DATA_FILES.downloaded_file,
					HEADERS.replenishment_style_sheet_headers[i], 0, i, "values didn't match");
		}

		// Verify the sheet has the styles
		for (int i = 1; i < HEADERS.replenishment_style_sheet_headers.length; i++) {
			jeevesvalidator.validateCellsAreNotEmpty(DATA_FILES.downloaded_file, 1, i, 1);
		}

	}

    @Test(enabled = true, groups = {
            "sanity", "order-indents" }, dataProvider = "MultiTsrTenants", dataProviderClass = buyingandplanningDP.class, description = "Tenant service tests")
    public void tenant_service_tests(String tenant)
            throws Exception {
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    	HashMap<String, String> create_baseoi_headers = jeeveshelper.initializeHeader(ROLES.CM_ROLE, "create");
    	String tenant_response = jeeveshelper.getTenantService(tenant, create_baseoi_headers);
    	TenantResponse response = mapper.readValue(tenant_response,TenantResponse.class);
    	List<TenantEntry> tenantEntry = response.getData();
    	jeevesvalidator.getTenantEntryAttributes(tenantEntry, tenant);
    	jeevesvalidator.getTenantDefinition(tenantEntry, tenant);  	
    	jeevesvalidator.getRconfiMetadata(tenantEntry);
    	jeevesvalidator.getWorkflowMetadata(tenantEntry, tenant);
    	jeevesvalidator.getWorkspaceMetadata(tenantEntry, tenant);
    	
    }
    
    
}
